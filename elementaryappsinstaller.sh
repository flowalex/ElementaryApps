#!/bin/bash

echo "Copyright (C) <2017>  <Alex Wolf> <github.com/flowalex999>
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    This script is designed to install all of the necessary files to compile some of the applications that are native on elementaryOS for Ubuntu based systems.  May build the application if it is a simple process."

echo "Do you accept the liscense terms [y/n]?"
read ans


if [ $ans = y -o $ans = Y -o $ans = yes -o $ans = Yes -o $ans = YES ]
then


echo "elementaryOS Terminal "

cd ~/elementary\ apps

git clone https://github.com/elementary/terminal.git
sleep 10s
sudo apt-get install libgranite-dev -y

sudo apt-get install libvte-2.91-dev -y

sudo apt-get install meson -y

sudo apt-get install valac -y


cd terminal

meson build --prefix=/usr

cd build
ninja test

sudo ninja install
io.elementary.terminal

echo "elementaryOS Code formerly Scratch)"
sleep 10s

cd ~/elementary\ apps

git clone https://github.com/elementary/code.git

sudo apt-get install libeditorconfig-dev -y

sudo apt-get install libgee-0.8-dev -y

sudo apt-get install libgtksourceview-3.0-dev -y

sudo apt-get install libgtkspell3-3-dev -y

sudo apt-get install libpeas-dev -y

sudo apt-get install libsoup2.4-dev -y

sudo apt-get install libvte-2.91-dev -y

sudo apt-get install libvala-0.36-dev -y

sudo apt-get install libwebkit2gtk-4.0-dev -y

sudo apt-get install libzeitgeist-2.0 -y

cd code

meson build --prefix=/usr
cd build
ninja test

sudo ninja install
io.elementary.code


echo "Music"

cd ~/elementary\ apps

git clone https://github.com/elementary/music.git

sudo apt-get install meson -y

sudo apt-get install libaccounts-glib-dev -y

sudo apt-get install libclutter-gtk-1.0-dev -y

sudo apt-get install libgda-5.0-dev -y

sudo apt-get install libgee-0.8-dev -y

sudo apt-get install  libglib2.0-dev -y

sudo apt-get install  libgpod-dev -y

sudo apt-get install  libgranite-dev -y

sudo apt-get install  libgsignon-glib-dev -y

sudo apt-get install  libgstreamer1.0-dev -y

sudo apt-get install  libgstreamer-plugins-base1.0-dev -y

sudo apt-get install  libgtk-3-dev -y

sudo apt-get install  libjson-glib-dev -y

sudo apt-get install  libnotify-dev -y

sudo apt-get install  libpeas-dev -y

sudo apt-get install  libsoup2.4-dev -y

sudo apt-get install  libtagc0-dev -y

sudo apt-get install libxml2-dev -y

sudo apt-get install  libzeitgeist-2.0-dev -y

sudo apt-get install  valac -y

cd music/
    
meson build --prefix=/usr

cd build

ninja

sudo ninja install




fi


if [ $ans = n -o $ans = N -o $ans = no -o $ans = No -o $ans = NO ]
then
echo "Exit"
exit
fi
